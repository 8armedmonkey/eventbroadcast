package sys.event;

public interface EventBroadcasterFactory {

    EventBroadcaster createEventBroadcaster();

    EventBroadcaster.CompositeSubscription createCompositeSubscription();

}
