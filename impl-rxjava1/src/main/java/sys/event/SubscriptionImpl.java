package sys.event;

import rx.Subscription;

class SubscriptionImpl implements EventBroadcaster.Subscription {

    final Subscription subscription;

    SubscriptionImpl(Subscription subscription) {
        this.subscription = subscription;
    }

    @Override
    public void unsubscribe() {
        subscription.unsubscribe();
    }

}
