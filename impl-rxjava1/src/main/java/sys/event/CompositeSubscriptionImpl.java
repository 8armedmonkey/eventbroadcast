package sys.event;

import rx.subscriptions.CompositeSubscription;

class CompositeSubscriptionImpl implements EventBroadcaster.CompositeSubscription {

    private CompositeSubscription compositeSubscription;

    CompositeSubscriptionImpl() {
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void unsubscribe() {
        compositeSubscription.unsubscribe();
    }

    @Override
    public void add(EventBroadcaster.Subscription subscription) {
        if (subscription instanceof SubscriptionImpl) {
            compositeSubscription.add(((SubscriptionImpl) subscription).subscription);
        } else {
            throw new IllegalArgumentException(String.format(
                    "Incompatible subscription. Expected: %s", SubscriptionImpl.class.getName()));
        }
    }

    @Override
    public void remove(EventBroadcaster.Subscription subscription) {
        if (subscription instanceof SubscriptionImpl) {
            compositeSubscription.remove(((SubscriptionImpl) subscription).subscription);
        } else {
            throw new IllegalArgumentException(String.format(
                    "Incompatible subscription. Expected: %s", SubscriptionImpl.class.getName()));
        }
    }

}
